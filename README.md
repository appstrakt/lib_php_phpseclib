phpseclib
=========

PHPSECLIB es una adecuación del proyecto PHPSECLIB de PHP para Symfony2. Podemos utilizarlo como un bundle.

Debemos tener en cuenta algo importante, por ahora, solo se puede hacer uso de la función TripleDES.

## Instalación y configuración:

Si haces uso de un archivo `deps` solo debes añadir:

    [PhpseclibBundle]
        git=http://github.com/jgabrielsinner10/phpseclib.git
	    target=/phpseclib

		
### En el archivo autoload.php se debe añadir al final del array pasado al registro de prefijos lo siguiente:

	$loader->registerPrefixes(array(
		'Twig_Extensions_' => __DIR__.'/../vendor/twig-extensions/lib',
		...
		'Phpseclib_'       => __DIR__.'/../vendor/phpseclib/lib',
	));

Vamos es Fácil...

## Utilizando el bundle PHPSecLib

Probemos con el ejemplo de guardar una sessión encriptada por medio del proceso de encriptación TripleDES.

	//Definimos nuestra llave de encriptación.
	$KEY_ENCRYPT = '548c286a61462d896573567b7a30335d4959427e5c7a675e325b6c7a7c';
	
	//Creamos nuestro objeto encriptador de la siguiente forma
	$encrypter = new \Phpseclib_TripleDES();
	
	//Pasamos la llave de encriptación.
	$encrypter->setKey($KEY_ENCRYPT);
	
	//Y entonces pudieramos incluso hasta encriptar un objeto usuario completo. haciendo lo siguiente.
	$this->session->set("usuario",$encrypter->encrypt(serialize($usuario_obj)));
	